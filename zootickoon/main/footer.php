<footer class="text-center mt-3 mt-auto">
    <div class="p-4 border-top d-flex justify-content-evenly">
        <a class="text-reset" href=".">Description</a>
        <a class="text-reset" href="animations.php">Animations</a>
        <a class="text-reset" href="#">Shop</a>
        <?php if (isset($_SESSION['email'])) { ?><a class="text-reset" href="afficheListeTickets.php">Tickets</a><?php } ?>
        <a class="text-reset" href="#">Wordpress</a>
    </div>
    <div class="p-4 border-top" style="background-color: rgba(0, 0, 0, 0.05);">
        © 2022 Copyright :
        <a class="text-reset fw-bold" href=".">Kévin Frick & Brayan Khali</a>
    </div>
</footer>