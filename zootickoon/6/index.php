<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="index.css" />
        <title>Ma page de test</title>
    </head>
    <body>
        <img src="images/test.png" alt="Mon image de test">
        <h1>
            Chapreisti
        </h1>
        <div class="secteurs">
            <div class="secteur">
                <h2>
                    Europe 
                </h2>
                <img src="../assets/lynx europe.jpg">
                <span class="code">
                    #Jmas
                </span>
                <p>
                    Ceci est une description
                </p>
                <span class="surface">
                    Tema la taille de la surface m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
            <div class="secteur">
                <h2>
                    Asie
                </h2>
                <img src="../assets/chat léopard asie.jpg">
                <span class="code">
                    #Thithuthuy
                </span>
                <p>
                    Ceci est une description
                    Ceci est une description
                    Ceci est une description
                    Ceci est une description
                    Ceci est une description
                </p>
                <span class="surface">
                    Tema la taille de la surface m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
        </div>
        <div class="secteurs">
            <div class="secteur">
                <h2>
                    Amerique
                </h2>
                <img src="../assets/Ocelot amerique.jpg">
                <span class="code">
                    #masnada
                </span>
                <p>
                    Ceci est une description
                </p>
                <span class="surface">
                    Tema la taille de la surface m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
            <div class="secteur">
                <h2>
                    Afrique 
                </h2>
                <img src="../assets/serval afrique.jpg">
                <span class="code">
                    #PPL
                </span>
                <p>
                    Ceci est une description
                </p>
                <span class="surface">
                    Tema la taille de la surface m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
        </div>
        <?php $heure = date("H"); 
            echo $heure; 
            if($heure>8 && $heure<13){
                echo '<img src="../assets/zebre.jpg">';
            }
            else if($heure>=13 && $heure<20){
                echo '<img src="../assets/girafe.jpg">';
            }
            else{
                echo '<img src="../assets/panda.jpg">';
            }   
            ?>
            <script type="text/javascript" src="function.js"></script>
    </body>
    
</html>